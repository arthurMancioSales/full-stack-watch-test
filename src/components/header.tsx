import { Clapperboard, MenuIcon } from "lucide-react";
import Link from "next/link";
import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";
import { Button } from "./ui/button";
import { useState } from "react";
import Cookies from "js-cookie";

export default function Header() {
  const [open, setOpen] = useState(false);

  return (
    <header>
      <div className="container mx-auto flex h-16 items-center justify-between px-4 md:px-6">
        <Link href="/dashboard" className="flex items-center" prefetch={false}>
          <Clapperboard className="size-6" />
          <span className="ml-2 text-lg font-medium">Watch now</span>
        </Link>
        <nav className="ml-auto hidden space-x-4 md:flex">
          <Link
            href="/dashboard"
            className="text-gray-700 hover:text-gray-900 focus:text-gray-900"
            prefetch={false}
          >
            Página inicial
          </Link>
          <Link
            href="/profile"
            className="text-gray-700 hover:text-gray-900 focus:text-gray-900"
            prefetch={false}
          >
            Perfil
          </Link>
          <Link
            href="/"
            className="text-gray-700 hover:text-gray-900 focus:text-gray-900"
            prefetch={false}
            onClick={() => {
              Cookies.remove("session");
            }}
          >
            Sair
          </Link>
        </nav>

        <Sheet open={open} onOpenChange={setOpen}>
          <SheetTrigger asChild>
            <Button variant="outline" size="icon" className="md:hidden">
              <MenuIcon className="size-6" />
              <span className="sr-only">Toggle navigation menu</span>
            </Button>
          </SheetTrigger>
          <SheetContent side="right">
            <div className="grid gap-4 p-4">
              <Link
                href="/dashboard"
                className="text-gray-700 hover:text-gray-900 focus:text-gray-900"
                prefetch={false}
                onClick={() => setOpen(false)}
              >
                Página inicial
              </Link>
              <Link
                href="/profile"
                className="text-gray-700 hover:text-gray-900 focus:text-gray-900"
                prefetch={false}
                onClick={() => setOpen(false)}
              >
                Perfil
              </Link>
              <Link
                href="/"
                className="text-gray-700 hover:text-gray-900 focus:text-gray-900"
                prefetch={false}
                onClick={() => {
                  Cookies.remove("session");
                }}
              >
                Sair
              </Link>
            </div>
          </SheetContent>
        </Sheet>
      </div>
    </header>
  );
}
