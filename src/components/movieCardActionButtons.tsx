import { Button } from "./ui/button";
import { Variants, motion } from "framer-motion";
import {
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuLabel,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { EllipsisVertical, Pencil, Trash } from "lucide-react";
import { useWizard } from "react-use-wizard";

export default function MovieCardActionButtons({ variants }: { variants: Variants }) {
  const { goToStep } = useWizard();
  return (
    <motion.div
      variants={variants}
      initial={{ opacity: 1 }}
      className="absolute left-4 top-0 z-20 flex gap-2 md:right-0"
    >
      <DropdownMenu>
        <DropdownMenuTrigger>
          <Button className="size-7 rounded-md p-1">
            <EllipsisVertical className="size-5" />
          </Button>
        </DropdownMenuTrigger>
        <DropdownMenuContent>
          <DropdownMenuLabel>Opções</DropdownMenuLabel>
          <DropdownMenuSeparator />
          <DropdownMenuItem onClick={() => goToStep(1)}>
            <Pencil className="mr-2 size-4" />
            Editar
          </DropdownMenuItem>
          <DropdownMenuItem onClick={() => goToStep(2)}>
            <Trash className="mr-2 size-4" />
            Excluir
          </DropdownMenuItem>
        </DropdownMenuContent>
      </DropdownMenu>
    </motion.div>
  );
}
