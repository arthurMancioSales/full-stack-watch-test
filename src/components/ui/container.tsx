import { cn } from "@/lib/utils";
import { HTMLAttributes } from "react";

interface containerProps extends HTMLAttributes<HTMLDivElement> {}

export default function Container({ className, children }: containerProps) {
  return (
    <div
      className={cn(
        className,
        "flex items-center justify-center mobileS:max-w-[270px] max-w-xs mx-auto md:max-w-xl lg:max-w-4xl xl:max-w-6xl 2xl:max-w-[1900px] w-full",
      )}
    >
      {children}
    </div>
  );
}
