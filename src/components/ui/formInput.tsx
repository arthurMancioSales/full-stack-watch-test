import * as React from "react";

import { cn } from "@/lib/utils";
import { Field } from "formik";
import { Eye, EyeOff } from "lucide-react";
import { AnimatePresence, HTMLMotionProps, motion } from "framer-motion";

const FormInputRoot = React.forwardRef<HTMLDivElement, React.HTMLAttributes<HTMLDivElement>>(
  ({ className, children, ...props }, ref) => {
    return (
      <div className={cn(className, "")} {...props} ref={ref} data-input-root>
        {children}
      </div>
    );
  },
);
FormInputRoot.displayName = "FormInputRoot";

export interface FormInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  errors: string[] | undefined;
  touched: boolean | undefined;
  name: string;
  icon?: React.ElementType;
  textarea?: boolean;
}

const FormInput = React.forwardRef<HTMLInputElement, FormInputProps>(
  ({ className, type, errors, touched, name, icon: Icon, textarea = false, ...props }, ref) => {
    const [showPassword, setShowPassword] = React.useState(false);
    const inputType = type === "password" && showPassword ? "text" : type;

    if (!errors) {
      errors = [];
    }

    return (
      <div className="relative">
        {Icon && (
          <div
            className={cn(
              "w-fit select-none absolute top-0 left-0 size-10 pointer-events-none opacity-80 p-2",
            )}
            data-input-icon-root
          >
            <Icon className="size-full" />
          </div>
        )}
        <Field
          name={name}
          type={inputType}
          className={cn(
            "flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50",
            className,
            Icon && "pl-10",
            touched && errors.includes(name) && "border-destructive border",
            touched && !errors.includes(name) && "border-emerald-700 border",
            textarea && "min-h-[80px]",
          )}
          data-input={name}
          ref={ref}
          {...props}
          as={textarea && "textarea"}
        />
        {type === "password" && (
          <div
            className="absolute right-0 top-0 z-10 w-fit p-2 text-foreground opacity-80 hover:cursor-pointer"
            onClick={(e) => {
              e.stopPropagation();
              setShowPassword(!showPassword);
            }}
          >
            {showPassword ? <Eye /> : <EyeOff />}
          </div>
        )}
      </div>
    );
  },
);
FormInput.displayName = "FormInput";

export interface FormInputLabelProps extends React.LabelHTMLAttributes<HTMLLabelElement> {
  required?: boolean;
}

const FormInputLabel = React.forwardRef<HTMLLabelElement, FormInputLabelProps>(
  ({ className, required, ...props }, ref) => {
    return (
      <label
        className={cn(
          "text-sm font-medium",
          className,
          required && "after:content-['*'] after:text-destructive",
        )}
        ref={ref}
        {...props}
        data-input-label
      />
    );
  },
);
FormInputLabel.displayName = "FormInputLabel";

export interface FormInputErrorProps extends HTMLMotionProps<"div"> {
  error: string | undefined;
  touched: boolean | undefined;
}

const FormInputError = React.forwardRef<HTMLDivElement, FormInputErrorProps>(
  ({ className, error, touched, ...props }, ref) => {
    return (
      <AnimatePresence>
        {touched && error && (
          <motion.div
            className={cn("text-sm text-destructive", className)}
            {...props}
            ref={ref}
            data-input-error
            key={`errorMessage`}
            initial={{ opacity: 0, height: 0 }}
            animate={{ opacity: 1, height: "auto" }}
            exit={{ opacity: 0, height: 0 }}
          >
            <motion.span className="text-sm text-destructive" data-error-message>
              {error}
            </motion.span>
          </motion.div>
        )}
      </AnimatePresence>
    );
  },
);
FormInputError.displayName = "FormInputError";

export { FormInputRoot, FormInput, FormInputLabel, FormInputError };
