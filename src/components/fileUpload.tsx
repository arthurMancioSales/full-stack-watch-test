import * as React from "react";
import { cn } from "@/lib/utils";
import Image from "next/image";

const FileUploadRoot = React.forwardRef<HTMLDivElement, React.HTMLAttributes<HTMLDivElement>>(
  ({ className, ...props }, ref) => (
    <div ref={ref} className={cn(`relative`, className)} {...props} />
  ),
);
FileUploadRoot.displayName = "FileUploadRoot";

interface FileUploadLabelProps extends React.HTMLAttributes<HTMLLabelElement> {
  error?: boolean;
}

const FileUploadInput = React.forwardRef<
  HTMLInputElement,
  React.InputHTMLAttributes<HTMLInputElement>
>(({ className, ...props }, ref) => (
  <input {...props} ref={ref} className={cn("hidden", className)} />
));
FileUploadInput.displayName = "FileUploadInput";

const FileUploadLabel = React.forwardRef<HTMLLabelElement, FileUploadLabelProps>(
  ({ className, error, ...props }, ref) => (
    <label
      {...props}
      ref={ref}
      className={cn(
        "size-16 cursor-pointer border border-input flex justify-center text-center items-center",
        error &&
          "border-destructive after:content-['Obrigatório'] text-destructive after:absolute after:bottom-0",
        className,
      )}
    />
  ),
);
FileUploadLabel.displayName = "FileUploadLabel";

interface FileUploadPreviewProps extends React.HTMLAttributes<HTMLImageElement> {
  image: string;
  alt: string;
}

const FileUploadPreview = React.forwardRef<HTMLImageElement, FileUploadPreviewProps>(
  ({ className, image, alt, ...props }, ref) => {
    if (image) {
      return (
        <div className="relative size-full" {...props}>
          <Image
            src={image}
            ref={ref}
            fill
            alt={alt}
            data-photo-preview
            className={cn("top-0 left-0 object-cover", className)}
          />
        </div>
      );
    }

    return props.children;
  },
);
FileUploadPreview.displayName = "FileUploadPreview";

export { FileUploadRoot, FileUploadLabel, FileUploadInput, FileUploadPreview };
