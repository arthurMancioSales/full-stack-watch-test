import { Form, Formik } from "formik";
import * as Yup from "yup";
import {
  FormInput,
  FormInputError,
  FormInputLabel,
  FormInputRoot,
} from "@/components/ui/formInput";
import { Message } from "@/lib/message";
import { Button } from "@/components/ui/button";
import axios, { AxiosResponse } from "axios";
import { toast } from "sonner";
import { ReactNode, useEffect, useState } from "react";
import { genre, genresModel, response } from "@/types";
import {
  DropdownMenu,
  DropdownMenuCheckboxItem,
  DropdownMenuContent,
  DropdownMenuTrigger,
} from "@/components/ui/dropdown-menu";
import { Badge } from "./ui/badge";
import { FileUploadInput, FileUploadLabel, FileUploadPreview, FileUploadRoot } from "./fileUpload";
import { decode } from "he";

export default function NewMovieForm({
  setOpenUpdateMovie,
  refreshMovies,
}: {
  setOpenUpdateMovie: (open: boolean) => void;
  refreshMovies: () => void;
}) {
  const [uploadImage, setUploadImage] = useState<string>("");
  const [uploadFile, setUploadFile] = useState<File | null>(null);

  const [genres, setGenres] = useState<genre[]>([]);
  const [, setLoading] = useState(true);

  const initialValues = {
    title: "",
    director: "",
    genres: [] as genre["id"][],
    poster: "",
    synopsis: "",
    year: 0,
  };

  const validationSchema = Yup.object().shape({
    title: Yup.string().required(Message.required),
    director: Yup.string().required(Message.required),
    genres: Yup.array().required(Message.required),
    poster: Yup.string().required(Message.required),
    synopsis: Yup.string().required(Message.required),
    year: Yup.number().required(Message.required),
  });

  useEffect(() => {
    async function getGenres() {
      try {
        const response: AxiosResponse<response<{ genres: genresModel[] }>> = await axios({
          method: "get",
          url: "/movies/get-genres",
          baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
          withCredentials: true,
          headers: {
            "Cache-Control": "no-cache",
          },
        });

        if (response.status === 200 && response.data.data) {
          setGenres(response.data.data.genres);
          setLoading(false);
        }
      } catch (error) {
        setLoading(false);
        if (axios.isAxiosError<response<null>>(error) && error.response) {
          return toast.error(error.response.data.errors[0].msg as ReactNode);
        }

        if (axios.isAxiosError<response<null>>(error) && error.cause) {
          return toast.error(error.cause.message);
        }

        return toast.error(Message.unexpectedError);
      }
    }

    getGenres();
  }, []);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={async (values, { setSubmitting, setFieldError, setFieldTouched }) => {
        setSubmitting(true);
        try {
          if (!uploadFile) {
            setFieldTouched("poster", true);
            setFieldError("poster", Message.required);
            return toast.error(Message.missingPoster);
          }

          const body = {
            title: values.title,
            director: values.director,
            genres: values.genres,
            poster: values.poster,
            synopsis: values.synopsis,
            year: values.year,
          };

          const form = new FormData();
          form.append("upload", uploadFile);
          const uploadedPoster: AxiosResponse<response<{ uploadedFile: string }>> = await axios({
            method: "post",
            url: "/upload",
            data: form,
            baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
            withCredentials: true,
          });

          if (uploadedPoster.status !== 200 || !uploadedPoster.data.data) {
            return toast.error(Message.errorOnFileUpload);
          }
          body.poster = decode(uploadedPoster.data.data.uploadedFile);

          const response: AxiosResponse<response<null>> = await axios({
            method: "post",
            url: "/movies/create-movie",
            data: body,
            baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
            withCredentials: true,
          });

          if (response.status === 200 && response.data.data) {
            toast.success("Filme criado com sucesso");
            setOpenUpdateMovie(false);
            refreshMovies();
          }
        } catch (error) {
          if (axios.isAxiosError<response<null>>(error) && error.response) {
            return toast.error(error.response.data.errors[0].msg as ReactNode);
          }

          if (axios.isAxiosError<response<null>>(error) && error.cause) {
            return toast.error(error.cause.message);
          }

          return toast.error(Message.unexpectedError);
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        isSubmitting,
        setFieldValue,
        setFieldTouched,
        setFieldError,
      }) => (
        <Form className="relative flex flex-col gap-8 px-3 pl-6">
          <div className="flex flex-col gap-4 md:grid md:grid-cols-2 md:grid-rows-2">
            <FormInputRoot className="h-fit md:col-start-1">
              <FormInputLabel required htmlFor="title">
                Título
              </FormInputLabel>
              <FormInput
                id="title"
                type="text"
                name="title"
                errors={Object.keys(errors)}
                touched={touched.title}
                value={values.title}
                onChange={(e) => {
                  setFieldTouched("title", true);
                  setFieldValue("title", e.target.value);
                }}
                required
              />
              <FormInputError error={errors.title} touched={touched.title} />
            </FormInputRoot>

            <FormInputRoot className="md:col-start-1">
              <FormInputLabel required htmlFor="director">
                Diretor
              </FormInputLabel>
              <FormInput
                id="director"
                type="text"
                name="director"
                errors={Object.keys(errors)}
                touched={touched.director}
                value={values.director}
                onChange={(e) => {
                  setFieldTouched("director", true);
                  setFieldValue("director", e.target.value);
                }}
                required
              />
              <FormInputError error={errors.director} touched={touched.director} />
            </FormInputRoot>

            <FormInputRoot className="md:col-start-1">
              <FormInputLabel required htmlFor="year">
                Ano de lançamento
              </FormInputLabel>
              <FormInput
                id="year"
                type="number"
                name="year"
                step={1}
                min={1800}
                max={new Date().getFullYear()}
                errors={Object.keys(errors)}
                touched={touched.year}
                value={values.year}
                onChange={(e) => {
                  setFieldTouched("year", true);
                  setFieldValue("year", e.target.value);
                }}
                required
              />
              <FormInputError error={errors.year} touched={touched.year} />
            </FormInputRoot>

            <FormInputRoot className="md:col-start-1">
              <FormInputLabel required htmlFor="synopsis">
                Sinópse
              </FormInputLabel>
              <FormInput
                id="synopsis"
                textarea
                type="textarea"
                name="synopsis"
                errors={Object.keys(errors)}
                touched={touched.synopsis}
                value={values.synopsis}
                onChange={(e) => {
                  setFieldTouched("synopsis", true);
                  setFieldValue("synopsis", e.target.value);
                }}
                required
              />
              <FormInputError error={errors.synopsis} touched={touched.synopsis} />
            </FormInputRoot>

            <FormInputRoot className="md:col-start-2 md:row-span-4 md:row-start-1">
              <FormInputLabel required htmlFor="poster">
                Capa do filme
              </FormInputLabel>
              <FileUploadRoot className="flex w-full">
                <FileUploadLabel className="aspect-[2/3] h-80 w-full flex-1 rounded-md">
                  <FileUploadInput
                    type="file"
                    accept="image/*"
                    onChange={(e) => {
                      setFieldTouched("poster", true);
                      if (e.target.files) {
                        const file = e.target.files[0];

                        const previewFile = URL.createObjectURL(file);

                        setUploadImage(previewFile);
                        setUploadFile(file);
                        setFieldValue("poster", previewFile);
                        return;
                      }
                      setFieldError("poster", Message.required);
                    }}
                  />
                  <FileUploadPreview
                    className="aspect-[2/3] w-full rounded-md"
                    alt="movie poster"
                    image={uploadImage}
                  >
                    <small className="text-xs">Escolher arquivo</small>
                  </FileUploadPreview>
                </FileUploadLabel>
              </FileUploadRoot>
              <FormInputError error={errors.poster} touched={touched.poster} />
            </FormInputRoot>

            <FormInputRoot className="md:col-span-2 md:col-start-1">
              <FormInputLabel required htmlFor="title">
                Categorias
              </FormInputLabel>

              <DropdownMenu>
                <DropdownMenuTrigger asChild>
                  <div
                    title="Clique para selecionar as categorias"
                    className="flex min-h-[80px] flex-wrap gap-1 rounded-md border bg-card p-2"
                  >
                    {values.genres.length > 0 &&
                      values.genres.map((genreId, i) => {
                        return (
                          <Badge key={i} className="mr-2 h-fit">
                            {genres.find((genre) => genre.id === genreId)?.name}
                          </Badge>
                        );
                      })}
                  </div>
                </DropdownMenuTrigger>
                <DropdownMenuContent className="w-56">
                  {Object.values(genres).map((genre) => {
                    return (
                      <DropdownMenuCheckboxItem
                        key={genre.id}
                        checked={values.genres.includes(genre.id)}
                        onCheckedChange={(checked: boolean) => {
                          setFieldTouched("genres", true);

                          if (checked) {
                            setFieldValue("genres", [...values.genres, genre.id]);
                            return;
                          }

                          setFieldValue(
                            "genres",
                            values.genres.filter((genreId) => genreId !== genre.id),
                          );
                        }}
                      >
                        {genre.name}
                      </DropdownMenuCheckboxItem>
                    );
                  })}
                </DropdownMenuContent>
              </DropdownMenu>
              <FormInputError error={errors.title} touched={touched.title} />
            </FormInputRoot>
          </div>

          <div className="flex flex-col gap-2">
            <Button
              type="submit"
              disabled={Object.keys(errors).length > 0}
              loading={isSubmitting}
              className="w-full"
            >
              Adicionar
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  );
}
