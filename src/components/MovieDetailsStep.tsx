import { movie } from "@/types";
import Image from "next/image";
import { Badge } from "./ui/badge";
import MovieCardActionButtons from "./movieCardActionButtons";
import { DrawerDescription, DrawerHeader, DrawerTitle } from "./ui/drawer";
import { Variants } from "framer-motion";
import { decode } from "he";

export default function MovieDetailsStep({
  movie,
  variants,
  isLargeScreen,
  isDesktop,
}: {
  movie: movie;
  variants: Variants;
  isLargeScreen: boolean;
  isDesktop: boolean;
}) {
  if (isDesktop) {
    return (
      <div className="relative flex h-full flex-col gap-8">
        <div className="absolute top-0 md:right-16">
          <MovieCardActionButtons variants={variants} />
        </div>
        <div className="flex gap-8">
          <div className="relative aspect-[2/3] h-[70vh] rounded-md shadow-lg">
            <Image
              src={decode(movie.poster)}
              alt={`${movie.title}`}
              fill
              className="rounded-md object-cover"
            />
          </div>

          <div className="flex flex-col gap-6">
            <div className="flex flex-col gap-2">
              <p className="text-xl font-medium">{movie.title}</p>
              <p className="text-foreground/70">{movie.year}</p>
            </div>

            <div className="flex flex-col gap-2">
              <p className="text-lg font-medium">Direção</p>
              <p className="text-foreground/70">{movie.director}</p>
            </div>

            <div className="col-start-2 flex flex-col gap-2">
              <p className="text-lg font-medium">Sinópse</p>
              <p className="text-foreground/70">{movie.synopsis}</p>
            </div>

            {isLargeScreen && (
              <div className="flex w-full flex-col gap-2">
                <p className="text-lg font-medium">Categorias</p>
                <div className="flex flex-wrap gap-2">
                  {movie.genres.map((genre) => {
                    return <Badge key={genre.id}>{genre.name}</Badge>;
                  })}
                </div>
              </div>
            )}
          </div>
        </div>

        {!isLargeScreen && (
          <div className="col-span-2 flex w-full flex-col gap-2">
            <p className="text-lg font-medium">Categorias</p>
            <div className="flex flex-wrap gap-2">
              {movie.genres.map((genre) => {
                return <Badge key={genre.id}>{genre.name}</Badge>;
              })}
            </div>
          </div>
        )}
      </div>
    );
  }

  return (
    <div className="relative flex flex-col gap-6 px-3 pl-6">
      <div className="absolute left-0 top-0 w-10 ">
        <MovieCardActionButtons variants={{}} />
      </div>
      <DrawerHeader className="p-0">
        <DrawerTitle>{movie.title}</DrawerTitle>
        <DrawerDescription>{movie.year}</DrawerDescription>
      </DrawerHeader>
      <div className="flex flex-col gap-2">
        <p className="text-lg font-medium">Direção</p>
        <p className="text-foreground/70">{movie.director}</p>
      </div>
      <div className="flex flex-col gap-2">
        <p className="text-lg font-medium">Sinópse</p>
        <p className="text-foreground/70">{movie.synopsis}</p>
      </div>
      <div className="flex flex-col gap-2">
        <p className="text-lg font-medium">Categorias</p>
        <div className="flex flex-wrap gap-2">
          {movie.genres.map((genre) => {
            return <Badge key={genre.id}>{genre.name}</Badge>;
          })}
        </div>
      </div>
    </div>
  );
}
