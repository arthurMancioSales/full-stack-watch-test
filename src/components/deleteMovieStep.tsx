import { movie, response } from "@/types";
import { DrawerHeader, DrawerTitle } from "./ui/drawer";
import { Button } from "./ui/button";
import { Trash, X } from "lucide-react";
import { useWizard } from "react-use-wizard";
import { ReactNode, useCallback, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { toast } from "sonner";
import { Message } from "@/lib/message";

export default function DeleteMovieStep({
  movie,
  setOpenUpdateMovie,
  refreshMovies,
}: {
  movie: movie;
  setOpenUpdateMovie: (open: boolean) => void;
  refreshMovies: () => void;
}) {
  const { goToStep } = useWizard();
  const [loading, setLoading] = useState(false);

  const deleteMovie = useCallback(
    async (movieId: string) => {
      setLoading(true);
      try {
        const response: AxiosResponse<response<null>> = await axios({
          method: "delete",
          url: `/movies/delete-movie`,
          data: {
            movieId,
          },
          baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
          withCredentials: true,
        });

        if (response.status === 200 && response.data.data) {
          setLoading(false);
          setOpenUpdateMovie(false);
          refreshMovies();
        }
      } catch (error) {
        setLoading(false);
        if (axios.isAxiosError<response<null>>(error) && error.response) {
          return toast.error(error.response.data.errors[0].msg.msg as ReactNode);
        }

        if (axios.isAxiosError<response<null>>(error) && error.cause) {
          return toast.error(error.cause.message);
        }

        return toast.error(Message.unexpectedError);
      }
    },
    [refreshMovies, setOpenUpdateMovie],
  );

  return (
    <div className="relative flex flex-col gap-6 px-3 pl-6">
      <DrawerHeader className="p-0">
        <DrawerTitle>Deletar Filme - {movie.title}</DrawerTitle>
      </DrawerHeader>
      <p className="text-center">
        Tem certeza que deseja apagar o filme {movie.title}? Essa operação não pode ser desfeita
      </p>
      <div className="mx-auto flex items-center gap-4">
        <Button
          onClick={() => {
            goToStep(0);
          }}
          disabled={loading}
        >
          <X className="mr-2 size-4" />
          Cancelar
        </Button>
        <Button
          variant={"destructive"}
          onClick={() => {
            deleteMovie(movie.id);
          }}
          disabled={loading}
        >
          <Trash className="mr-2 size-4" />
          Excluir
        </Button>
      </div>
    </div>
  );
}
