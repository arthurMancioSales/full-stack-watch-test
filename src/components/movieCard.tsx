import { movie } from "@/types";
import { Drawer, DrawerContent, DrawerTrigger } from "@/components/ui/drawer";
import { useMediaQuery } from "react-responsive";
import Image from "next/image";
import { Variants, motion } from "framer-motion";
import { ScrollArea } from "./ui/scroll-area";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";
import { useState } from "react";
import { Wizard } from "react-use-wizard";
import MovieDetailsStep from "./MovieDetailsStep";
import UpdateMovieForm from "./updateMovieForm";
import { decode } from "he";
import DeleteMovieStep from "./deleteMovieStep";

export default function MovieCard({
  movie,
  variants,
  refreshMovies,
}: {
  movie: movie;
  variants: Variants;
  refreshMovies: () => void;
}) {
  const isDesktop = useMediaQuery({ query: "(min-width: 768px)" });
  const isLargeScreen = useMediaQuery({ query: "(min-width: 1024px)" });

  const [openMovie, setOpenMovie] = useState(false);

  if (isDesktop) {
    return (
      <Dialog modal open={openMovie} onOpenChange={setOpenMovie}>
        <DialogTrigger>
          <motion.div
            variants={variants}
            className="relative aspect-[2/3] w-full rounded-md shadow-lg"
            whileHover={"visible"}
          >
            <Image
              src={decode(movie.poster)}
              alt={`${movie.title}`}
              fill
              className="rounded-md object-cover"
            />
            <div className="absolute bottom-0 flex w-full flex-col items-start justify-center rounded-md rounded-t-none bg-secondary-foreground/80 p-2 text-white/80 backdrop-blur-sm">
              <h5 className="font-medium">{movie.title}</h5>
              <h6 className="text-sm">{movie.year}</h6>
            </div>
          </motion.div>
        </DialogTrigger>
        <DialogContent
          overlay={false}
          className="flex h-[80vh] max-h-[80vh] min-h-[30vh] w-[80vw] max-w-[auto] bg-card/80 pr-5 backdrop-blur-md xl:max-w-[70vw] 2xl:max-w-[60vw]"
        >
          <div className="pointer-events-auto flex h-full flex-1">
            <ScrollArea type="always" className="size-full">
              <Wizard>
                <MovieDetailsStep
                  movie={movie}
                  isLargeScreen={isLargeScreen}
                  isDesktop={isDesktop}
                  variants={variants}
                />
                <UpdateMovieForm
                  movie={movie}
                  setOpenUpdateMovie={setOpenMovie}
                  refreshMovies={refreshMovies}
                />
                <DeleteMovieStep
                  movie={movie}
                  setOpenUpdateMovie={setOpenMovie}
                  refreshMovies={refreshMovies}
                />
              </Wizard>
            </ScrollArea>
          </div>
        </DialogContent>
      </Dialog>
    );
  }

  return (
    <Drawer open={openMovie} onOpenChange={setOpenMovie}>
      <DrawerTrigger>
        <motion.div
          variants={variants}
          className="relative aspect-[2/3] w-full rounded-md shadow-lg"
        >
          <Image
            src={decode(movie.poster)}
            alt={`${movie.title}`}
            fill
            className="rounded-md object-cover"
          />
          <div className="absolute bottom-0 flex w-full flex-col items-start justify-center rounded-md rounded-t-none bg-secondary-foreground/80 p-2 text-white/80 backdrop-blur-sm">
            <h5 className="text-left font-medium">{movie.title}</h5>
            <h6 className="text-sm">{movie.year}</h6>
          </div>
        </motion.div>
      </DrawerTrigger>
      <DrawerContent
        overlay={false}
        className="h-[60vh] max-h-[60vh] min-h-[30vh] bg-card/80 backdrop-blur-md"
      >
        <div className="pointer-events-auto mr-3 flex h-4/5 flex-1">
          <ScrollArea type="always" className="w-full">
            <div className="py-6">
              <Wizard>
                <MovieDetailsStep
                  movie={movie}
                  isLargeScreen={isLargeScreen}
                  isDesktop={isDesktop}
                  variants={variants}
                />
                <UpdateMovieForm
                  movie={movie}
                  setOpenUpdateMovie={setOpenMovie}
                  refreshMovies={refreshMovies}
                />
                <DeleteMovieStep
                  movie={movie}
                  setOpenUpdateMovie={setOpenMovie}
                  refreshMovies={refreshMovies}
                />
              </Wizard>
            </div>
          </ScrollArea>
        </div>
      </DrawerContent>
    </Drawer>
  );
}
