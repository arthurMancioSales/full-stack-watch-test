import { Form, Formik } from "formik";
import * as Yup from "yup";
import {
  FormInput,
  FormInputError,
  FormInputLabel,
  FormInputRoot,
} from "@/components/ui/formInput";
import { Message } from "@/lib/message";
import { Button } from "@/components/ui/button";
import axios, { AxiosResponse } from "axios";
import { toast } from "sonner";
import { ReactNode, useState } from "react";
import { response, user } from "@/types";
import Cookies from "js-cookie";
import { Mail, Lock, SquareArrowOutUpRight, Trash } from "lucide-react";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";
import { useRouter } from "next/navigation";

export default function UpdateUserForm({
  user,
  setUser,
}: {
  user: user;
  setUser: (updatedUser: user) => void;
}) {
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const [openDeleteAccount, setOpenDeleteAccount] = useState(false);

  const initialValues = {
    email: user.email,
    oldPassword: "",
    newPassword: "",
    passwordForDelete: "",
  };
  const validationSchema = Yup.object().shape({
    email: Yup.string().email(Message.invalidEmail).required(Message.required),
    oldPassword: Yup.string().required(Message.required),
    newPassword: Yup.string().required(Message.required),
    passwordForDelete: Yup.string().required(Message.required),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={async (values) => {
        setLoading(true);

        try {
          const response: AxiosResponse<response<{ token: string }>> = await axios({
            method: "post",
            url: "/session/update-user",
            data: {
              email: values.email,
              oldPassword: values.oldPassword,
              newPassword: values.newPassword,
              id: user.id,
            },
            baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
            headers: {
              "Cache-Control": "no-cache",
            },
            withCredentials: true,
          });

          if (response.status === 200 && response.data.data) {
            const userToken: user = JSON.parse(
              Buffer.from(response.data.data.token.split(".")[1], "base64").toString(),
            );

            setUser(userToken);

            Cookies.set("session", response.data.data.token, { expires: 14 });

            setLoading(false);

            toast.success("Usuário atualizado com sucesso");
          }
        } catch (error) {
          setLoading(false);

          if (axios.isAxiosError<response<null>>(error) && error.response) {
            return toast.error(error.response.data.errors[0].msg as ReactNode);
          }

          if (axios.isAxiosError<response<null>>(error) && error.cause) {
            return toast.error(error.cause.message);
          }

          return toast.error(Message.unexpectedError);
        }
      }}
    >
      {({ errors, touched, values }) => (
        <Form className="flex flex-col gap-8">
          <div className="flex flex-col gap-4">
            <FormInputRoot>
              <FormInputLabel required htmlFor="email">
                Email
              </FormInputLabel>
              <FormInput
                id="email"
                type="email"
                name="email"
                errors={Object.keys(errors)}
                touched={touched.email}
                required
                icon={Mail}
              />
              <FormInputError error={errors.email} touched={touched.email} />
            </FormInputRoot>

            <FormInputRoot>
              <FormInputLabel required htmlFor="oldPassword">
                Senha anterior
              </FormInputLabel>
              <FormInput
                id="oldPassword"
                type="password"
                name="oldPassword"
                errors={Object.keys(errors)}
                touched={touched.oldPassword}
                required
                icon={Lock}
              />
              <FormInputError error={errors.oldPassword} touched={touched.oldPassword} />
            </FormInputRoot>

            <FormInputRoot>
              <FormInputLabel required htmlFor="newPassword">
                Nova senha
              </FormInputLabel>
              <FormInput
                id="newPassword"
                type="password"
                name="newPassword"
                errors={Object.keys(errors)}
                touched={touched.newPassword}
                required
                icon={Lock}
              />
              <FormInputError error={errors.newPassword} touched={touched.newPassword} />
            </FormInputRoot>
          </div>

          <div className="flex flex-col gap-2">
            <Button
              type="submit"
              disabled={Object.keys(errors).length > 0}
              loading={loading}
              className="w-full"
            >
              Atualizar
            </Button>
            <Dialog modal open={openDeleteAccount} onOpenChange={setOpenDeleteAccount}>
              <DialogTrigger>
                <Button variant={"destructive"} className="w-full">
                  <SquareArrowOutUpRight className="mr-2 size-4" />
                  Excluir conta
                </Button>
              </DialogTrigger>
              <DialogContent
                overlay={false}
                className="flex h-fit w-[80vw] max-w-[auto] rounded-md bg-card/80 pr-5 backdrop-blur-md xl:max-w-[70vw] 2xl:max-w-[60vw]"
              >
                <div className="pointer-events-auto flex h-full flex-1 flex-col items-center gap-6">
                  <h3 className="text-xl font-bold text-destructive">Apagar conta</h3>
                  <p>
                    Tem certeza que deseja apagar sua conta? Essa operação não pode ser desfeita.
                  </p>

                  <FormInputRoot>
                    <FormInputLabel required htmlFor="passwordForDelete">
                      Senha
                    </FormInputLabel>
                    <FormInput
                      id="passwordForDelete"
                      type="password"
                      name="passwordForDelete"
                      errors={Object.keys(errors)}
                      touched={touched.passwordForDelete}
                      required
                      icon={Lock}
                    />
                    <FormInputError
                      error={errors.passwordForDelete}
                      touched={touched.passwordForDelete}
                    />
                  </FormInputRoot>

                  <Button
                    variant="destructive"
                    type="button"
                    disabled={!values.passwordForDelete || !!errors.passwordForDelete}
                    onClick={async () => {
                      try {
                        const response: AxiosResponse<response<null>> = await axios({
                          method: "delete",
                          url: "/session/delete-user",
                          data: {
                            id: user.id,
                            password: values.passwordForDelete,
                          },
                          baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
                          headers: {
                            "Cache-Control": "no-cache",
                          },
                          withCredentials: true,
                        });

                        if (response.status === 200) {
                          toast.success("Conta apagada com sucesso");

                          Cookies.remove("session");

                          router.replace("/");
                        }
                      } catch (error) {
                        if (axios.isAxiosError<response<null>>(error) && error.response) {
                          return toast.error(error.response.data.errors[0].msg as ReactNode);
                        }

                        if (axios.isAxiosError<response<null>>(error) && error.cause) {
                          return toast.error(error.cause.message);
                        }

                        return toast.error(Message.unexpectedError);
                      }
                    }}
                  >
                    <Trash className="mr-2 size-4" />
                    Confirmar
                  </Button>
                </div>
              </DialogContent>
            </Dialog>
          </div>
        </Form>
      )}
    </Formik>
  );
}
