import { Drawer, DrawerContent, DrawerTrigger } from "@/components/ui/drawer";
import { useMediaQuery } from "react-responsive";
import { ScrollArea } from "./ui/scroll-area";
import { Dialog, DialogContent, DialogTrigger } from "@/components/ui/dialog";
import { useState } from "react";
import { Button } from "./ui/button";
import NewMovieForm from "./newMovieForm";

export default function NewMovieCard({ refreshMovies }: { refreshMovies: () => void }) {
  const isDesktop = useMediaQuery({ query: "(min-width: 768px)" });

  const [openMovie, setOpenMovie] = useState(false);

  if (isDesktop) {
    return (
      <Dialog modal open={openMovie} onOpenChange={setOpenMovie}>
        <DialogTrigger>
          <Button>Adicionar novo filme</Button>
        </DialogTrigger>
        <DialogContent
          overlay={false}
          className="flex h-[80vh] max-h-[80vh] min-h-[30vh] w-[80vw] max-w-[auto] bg-card/80 pr-5 backdrop-blur-md xl:max-w-[70vw] 2xl:max-w-[60vw]"
        >
          <div className="pointer-events-auto flex h-full flex-1">
            <ScrollArea type="always" className="size-full">
              <NewMovieForm refreshMovies={refreshMovies} setOpenUpdateMovie={setOpenMovie} />
            </ScrollArea>
          </div>
        </DialogContent>
      </Dialog>
    );
  }

  return (
    <Drawer open={openMovie} onOpenChange={setOpenMovie}>
      <DrawerTrigger>
        <Button>Adicionar novo filme</Button>
      </DrawerTrigger>
      <DrawerContent
        overlay={false}
        className="h-[60vh] max-h-[60vh] min-h-[30vh] bg-card/80 backdrop-blur-md"
      >
        <div className="pointer-events-auto mr-3 flex h-4/5 flex-1">
          <ScrollArea type="always" className="w-full">
            <div className="py-6">
              <NewMovieForm refreshMovies={refreshMovies} setOpenUpdateMovie={setOpenMovie} />
            </div>
          </ScrollArea>
        </div>
      </DrawerContent>
    </Drawer>
  );
}
