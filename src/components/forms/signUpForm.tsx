"use client";

import { Form, Formik } from "formik";
import * as Yup from "yup";
import { FormInput, FormInputError, FormInputLabel, FormInputRoot } from "../ui/formInput";
import { Lock, Mail } from "lucide-react";
import { Message } from "@/lib/message";
import { Button } from "../ui/button";
import Link from "next/link";
import axios, { AxiosResponse } from "axios";
import { toast } from "sonner";
import { ReactNode } from "react";
import { useRouter } from "next/navigation";
import Cookies from "js-cookie";
import { response } from "@/types";

export default function SignUpForm() {
  const router = useRouter();

  const initialValues = {
    email: "",
    password: "",
    passwordConfirmation: "",
  };

  const validationSchema = Yup.object().shape({
    email: Yup.string().email(Message.invalidEmail).required(Message.required),
    password: Yup.string().required(Message.required),
    passwordConfirmation: Yup.string()
      .oneOf([Yup.ref("password")], Message.passwordMismatch)
      .required(Message.required),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={async (values, { setSubmitting }) => {
        setSubmitting(true);

        try {
          const response: AxiosResponse<response<{ token: string }>> = await axios({
            method: "post",
            url: "/session/sign-up",
            data: {
              email: values.email,
              password: values.password,
              passwordConfirmation: values.passwordConfirmation,
            },
            baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
            headers: {
              "Cache-Control": "no-cache",
            },
          });

          if (response.status === 200 && response.data.data) {
            Cookies.set("session", response.data.data.token, { expires: 14 });
            toast.success("Cadastro realizado com sucesso");
            router.replace("/dashboard");
          }
        } catch (error) {
          if (axios.isAxiosError<response<null>>(error) && error.response) {
            return toast.error(error.response.data.errors[0].msg as ReactNode);
          }

          if (axios.isAxiosError<response<null>>(error) && error.cause) {
            return toast.error(error.cause.message);
          }

          return toast.error(Message.unexpectedError);
        }
      }}
    >
      {({ values, errors, touched, isSubmitting }) => (
        <Form className="flex flex-col gap-8">
          <div className="flex flex-col gap-4">
            <FormInputRoot>
              <FormInputLabel required htmlFor="email">
                Email
              </FormInputLabel>
              <FormInput
                id="email"
                type="email"
                name="email"
                errors={Object.keys(errors)}
                touched={touched.email}
                required
                icon={Mail}
              />
              <FormInputError error={errors.email} touched={touched.email} />
            </FormInputRoot>

            <FormInputRoot>
              <FormInputLabel required htmlFor="password">
                Senha
              </FormInputLabel>
              <FormInput
                id="password"
                type="password"
                name="password"
                errors={Object.keys(errors)}
                touched={touched.password}
                required
                icon={Lock}
              />
              <FormInputError error={errors.password} touched={touched.password} />
            </FormInputRoot>

            <FormInputRoot>
              <FormInputLabel required htmlFor="passwordConfirmation">
                Confirmação de senha
              </FormInputLabel>
              <FormInput
                id="passwordConfirmation"
                type="password"
                name="passwordConfirmation"
                errors={Object.keys(errors)}
                touched={touched.passwordConfirmation}
                required
                icon={Lock}
              />
              <FormInputError
                error={errors.passwordConfirmation}
                touched={touched.passwordConfirmation}
              />
            </FormInputRoot>
          </div>

          <div className="flex flex-col gap-2">
            <Button
              type="submit"
              disabled={
                Object.keys(errors).length > 0 ||
                Object.keys(touched).length !== Object.keys(values).length
              }
              loading={isSubmitting}
              className="w-full"
            >
              Entrar
            </Button>
            <Link href="/">
              <p className="text-sm hover:text-foreground/90">Já possui uma conta? Faça login</p>
            </Link>
          </div>
        </Form>
      )}
    </Formik>
  );
}
