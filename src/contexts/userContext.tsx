import { userContextProps } from "@/types";
import { createContext } from "react";

export const userContext = createContext<userContextProps | null>(null);
