import { ValidationError } from "express-validator";

export type response<T> = {
  data: T | null;
  errors: { msg: string }[] | ValidationError[];
};

export type user = {
  id: string;
  name: string;
  email: string;
};

export type userContextProps = {
  user: user | null;
  setUser: (user: user) => void;
};

export enum genresList {
  action = "ação",
  adventure = "aventura",
  comedy = "comédia",
  drama = "drama",
  fantasy = "fantasia",
  horror = "terror",
  mystery = "mistério",
  romance = "romance",
  sciFi = "ficção científica",
  thriller = "suspense",
  animation = "animação",
  crime = "policial",
}

export type genresModel = {
  id: string;
  name: genresList;
};

export type genre = {
  id: string;
  name: genresList;
};

export type movie = {
  id: string;
  title: string;
  director: string;
  genres: genre[];
  year: number;
  poster: string;
  synopsis: string;
};
