export const Message = {
  // Form validation messages
  invalidEmail: "Email inválido",
  required: "Campo obrigatório",
  passwordTooWeak:
    "Senha deve ter ao menos 6 caracteres, uma letra maiúscula, uma minúscula e um número",
  passwordMismatch: "Senhas não conferem",
  missingId: "Id obrigatório",

  // API error messages
  unexpectedError: "Um erro inesperado ocorreu. Por favor, tente novamente mais tarde.",
  accessDenied: "Acesso negado",
  errorOnFileUpload: "Erro ao fazer upload do arquivo",
  unauthorized: "Não autorizado",

  // sign-in
  invalidCredentials: "Credenciais inválidas",
  userNotFound: "Usuário não encontrado",
  missingEmail: "Email obrigatório",
  missingPassword: "Senha obrigatória",

  // sign-up
  emailAlreadyExists: "Email já cadastrado",

  // movie
  noMovieFound: "Nenhum filme encontrado",
  noGenreFound: "Nenhum gênero encontrado",
  movieNotFound: "Filme não encontrado",
  movieAlreadyExists: "Filme já cadastrado",
  movieCreationFailed: "Falha ao criar filme",

  missingTitle: "Título obrigatório",
  missingGenre: "Gênero obrigatório",
  missingYear: "Ano obrigatório",
  missingDirector: "Diretor obrigatório",
  missingSynopsis: "Sinopse obrigatória",
  missingPoster: "Poster obrigatório",
};
