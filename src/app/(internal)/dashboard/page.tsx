"use client";

import MovieCard from "@/components/movieCard";
import Container from "@/components/ui/container";
import { Message } from "@/lib/message";
import { movie, response } from "@/types";
import axios, { AxiosResponse } from "axios";
import { ReactNode, useCallback, useEffect, useState } from "react";
import { toast } from "sonner";
import {
  Pagination,
  PaginationContent,
  PaginationEllipsis,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from "@/components/ui/pagination";
import { Events, animateScroll as scroll, scrollSpy } from "react-scroll";
import { motion, Variants } from "framer-motion";
import { useMediaQuery } from "react-responsive";
import NewMovieCard from "@/components/newMovieCard";

export const fetchCache = "default-no-store";

export default function DashboardPage({
  searchParams,
}: {
  searchParams: { [key: string]: string | string[] | undefined };
}) {
  const [movies, setMovies] = useState<movie[]>([]);
  const [currentPage, setCurrentPage] = useState(parseInt(searchParams.page as string) || 1);
  const [moviesByPage] = useState(parseInt(searchParams.take as string) || 10);
  const [, setLoading] = useState(true);

  const isDesktop = useMediaQuery({ query: "(min-width: 768px)" });

  const containerVariants: Variants = {
    initial: { opacity: 1, scale: "50%", transition: { staggerChildren: 0.1, delayChildren: 0.3 } },
    hidden: { opacity: 0 },
    visible: { opacity: 1 },
    animate: {
      opacity: 1,
      scale: "100%",
      transition: { staggerChildren: 20, delayChildren: 0.3 },
    },
  };

  const childVariants: Variants = {
    initial: { opacity: 0, scale: "50%" },
    animate: {
      opacity: 1,
      scale: "100%",
    },
  };

  const getMovies = useCallback(async () => {
    setLoading(true);
    try {
      const response: AxiosResponse<response<{ movies: movie[] }>> = await axios({
        method: "get",
        url: `/movies/get-movies?skip=${moviesByPage * (currentPage - 1)}&take=${moviesByPage}`,
        baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
        withCredentials: true,
        headers: {
          "Cache-Control": "no-cache",
        },
      });

      if (response.status === 200 && response.data.data) {
        setLoading(false);
        setMovies(response.data.data.movies);
      }
    } catch (error) {
      setLoading(false);
      if (axios.isAxiosError<response<null>>(error) && error.response) {
        return toast.error(error.response.data.errors[0].msg as ReactNode);
      }

      if (axios.isAxiosError<response<null>>(error) && error.cause) {
        return toast.error(error.cause.message);
      }

      return toast.error(Message.unexpectedError);
    }
  }, [currentPage, moviesByPage]);

  useEffect(() => {
    getMovies();
    Events.scrollEvent.register("begin", () => {});

    Events.scrollEvent.register("end", () => {});

    scrollSpy.update();

    return () => {
      Events.scrollEvent.remove("begin");
      Events.scrollEvent.remove("end");
    };
  }, [currentPage, getMovies, moviesByPage]);

  return (
    <Container className="flex min-h-screen flex-col gap-8 py-8">
      <div className="flex flex-col gap-2">
        <h2 className="text-4xl font-bold">Seja bem-vindo novamente!</h2>
        <p className=" text-sm">Clique na capa de um filme para saber mais sobre ele</p>
      </div>
      <motion.div
        variants={containerVariants}
        className="grid w-full grid-cols-2 gap-4 lg:grid-cols-5 lg:gap-8"
        initial={"initial"}
        animate={"animate"}
      >
        {movies.length === 0 && <p>Nenhum filme encontrado</p>}
        {movies.length > 0 &&
          movies.map((movie) => {
            return (
              <MovieCard
                key={movie.id}
                movie={movie}
                variants={childVariants}
                refreshMovies={() => getMovies()}
              />
            );
          })}
      </motion.div>

      {!isDesktop && (
        <Pagination>
          <PaginationContent>
            <PaginationItem>
              <PaginationPrevious
                href={currentPage === 1 ? "#" : `?page=${currentPage}&take=${moviesByPage}`}
                onClick={() => {
                  if (currentPage === 1) return;
                  scroll.scrollToTop();
                  setCurrentPage(currentPage - 1);
                }}
              />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink isActive={true} href="#">
                {currentPage}
              </PaginationLink>
            </PaginationItem>

            <PaginationItem>
              <PaginationNext
                href={movies.length !== 0 ? `?page=${currentPage + 1}&take=${moviesByPage}` : "#"}
                onClick={() => {
                  scroll.scrollToTop();
                  if (movies.length === 0) return;
                  setCurrentPage(currentPage + 1);
                }}
              />
            </PaginationItem>
          </PaginationContent>
        </Pagination>
      )}

      {isDesktop && (
        <Pagination>
          <PaginationContent>
            <PaginationItem>
              <PaginationPrevious
                href={currentPage === 1 ? "#" : `?page=${currentPage}&take=${moviesByPage}`}
                onClick={() => {
                  if (currentPage === 1) return;
                  scroll.scrollToTop();
                  setCurrentPage(currentPage - 1);
                }}
              />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink isActive={currentPage === 1} href="#">
                {1}
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink isActive={currentPage === 0} href="#">
                {currentPage}
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink isActive={currentPage === 0} href="#">
                {currentPage}
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationEllipsis />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink isActive={currentPage === 0} href="#">
                {currentPage}
              </PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationNext
                href={`?page=${currentPage}&take=${moviesByPage}`}
                onClick={() => {
                  setCurrentPage(currentPage + 1);
                  scroll.scrollToTop();
                }}
              />
            </PaginationItem>
          </PaginationContent>
        </Pagination>
      )}
      <div className="flex flex-col gap-1 text-center">
        <p>Não encontrou o que você procurava? Adicione um novo filme agora mesmo!</p>
        <NewMovieCard refreshMovies={() => getMovies()} />
      </div>
    </Container>
  );
}
