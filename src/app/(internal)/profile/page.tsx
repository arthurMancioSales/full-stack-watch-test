"use client";

import Container from "@/components/ui/container";
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card";
import { motion } from "framer-motion";
import { userContext } from "@/contexts/userContext";
import { useContext } from "react";
import { user, userContextProps } from "@/types";
import UpdateUserForm from "@/components/UpdateUserForm";
import { Skeleton } from "@/components/ui/skeleton";

export default function UserProfilePage() {
  const context = useContext<userContextProps | null>(userContext);

  if (!context || !context.user) {
    return (
      <div className="flex min-h-screen items-center justify-center bg-[url('/authBackground.svg')] bg-cover bg-center">
        <Container>
          <div className="w-full max-w-md rounded-lg bg-white p-6 shadow-md dark:bg-gray-900 sm:p-8">
            <div className="flex flex-col items-center">
              <Skeleton className="mb-4 size-12 rounded-full" />
              <div className="space-y-2 text-center">
                <Skeleton className="h-6 w-32" />
                <Skeleton className="h-4 w-24" />
              </div>
              <div className="mt-6 w-full">
                <div className="space-y-4">
                  <div className="grid grid-cols-2 gap-4">
                    <div className="space-y-2">
                      <Skeleton className="h-4 w-20" />
                      <Skeleton className="h-8 w-full" />
                    </div>
                    <div className="space-y-2">
                      <Skeleton className="h-4 w-20" />
                      <Skeleton className="h-8 w-full" />
                    </div>
                  </div>
                  <div className="space-y-2">
                    <Skeleton className="h-4 w-20" />
                    <Skeleton className="h-20 w-full" />
                  </div>
                </div>
                <div className="mt-6 flex justify-end gap-2">
                  <Skeleton className="h-8 w-20" />
                  <Skeleton className="h-8 w-20" />
                </div>
              </div>
            </div>
          </div>
        </Container>
      </div>
    );
  }

  return (
    <div className="flex items-center bg-[url('/authBackground.svg')] bg-cover bg-center">
      <Container className="min-h-screen">
        <motion.div
          initial={{ opacity: 0, x: -50 }}
          animate={{ opacity: 1, x: 0 }}
          transition={{ duration: 0.3 }}
          className="flex items-center justify-center"
        >
          <Card className="flex w-full max-w-sm flex-col items-center bg-card/75 backdrop-blur-lg">
            <CardHeader className="self-center">
              <CardTitle className="text-center">Perfil</CardTitle>
            </CardHeader>
            <CardContent className="min-h-[60vh] w-full ">
              <UpdateUserForm
                user={context.user}
                setUser={(updatedUser: user) => context.setUser(updatedUser)}
              />
            </CardContent>
          </Card>
        </motion.div>
      </Container>
    </div>
  );
}
