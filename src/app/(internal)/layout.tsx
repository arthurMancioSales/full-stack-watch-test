"use client";
import { ReactNode, useEffect, useMemo, useState } from "react";
import cookies from "js-cookie";
import axios, { AxiosResponse } from "axios";
import { response, user } from "@/types";
import { useRouter } from "next/navigation";
import { toast } from "sonner";
import { Message } from "@/lib/message";
import { userContext } from "@/contexts/userContext";
import Header from "@/components/header";

export default function InternalLayout({ children }: { children: React.ReactNode }) {
  const [user, setUser] = useState<user | null>(null);

  const router = useRouter();

  useEffect(() => {
    const token = cookies.get("session");

    if (!token) {
      return router.replace("/");
    }

    const userToken: user = JSON.parse(Buffer.from(token.split(".")[1], "base64").toString());

    async function validateUser() {
      try {
        const response: AxiosResponse<response<{ isValid: boolean }>> = await axios({
          method: "get",
          url: "/session/validate-session",
          baseURL: process.env.NEXT_PUBLIC_BACKEND_ADDRESS,
          withCredentials: true,
          headers: {
            "Cache-Control": "no-cache",
          },
        });

        if (response.status === 200 && response.data.data) {
          setUser(userToken);
        }
      } catch (error) {
        if (axios.isAxiosError<response<null>>(error) && error.response) {
          cookies.remove("token");
          router.replace("/");
          return toast.error(error.response.data.errors[0].msg as ReactNode);
        }

        if (axios.isAxiosError<response<null>>(error) && error.cause) {
          return toast.error(error.cause.message);
        }

        return toast.error(Message.unexpectedError);
      }
    }

    validateUser();
  }, [router]);

  const contextValue = useMemo(
    () => ({
      user,
      setUser,
    }),
    [user, setUser],
  );

  return (
    <userContext.Provider value={contextValue}>
      <div className=" bg-[url('/dashboardBackground.svg')] bg-cover bg-fixed bg-center">
        <Header />
        {children}
      </div>
    </userContext.Provider>
  );
}
