"use client";

import Container from "@/components/ui/container";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";
import { motion } from "framer-motion";
import SignInForm from "@/components/forms/SignInForm";

export default function SignInPage() {
  return (
    <div className="flex items-center bg-[url('/authBackground.svg')] bg-cover bg-center">
      <Container className="min-h-screen">
        <motion.div
          initial={{ opacity: 0, x: -50 }}
          animate={{ opacity: 1, x: 0 }}
          transition={{ duration: 0.3 }}
          className="flex items-center justify-center"
        >
          <Card className="flex w-full max-w-sm flex-col items-center bg-card/75 backdrop-blur-lg">
            <CardHeader className="self-start">
              <CardTitle>Entrar</CardTitle>
              <CardDescription>Insira seu email e senha para acessar a plataforma</CardDescription>
            </CardHeader>
            <CardContent className="w-full">
              <SignInForm />
            </CardContent>
          </Card>
        </motion.div>
      </Container>
    </div>
  );
}
