"use client";

import Container from "@/components/ui/container";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";
import SignUpForm from "@/components/forms/signUpForm";
import { motion } from "framer-motion";

export default function SignUpPage() {
  return (
    <div className="flex items-center bg-[url('/authBackground.svg')] bg-cover bg-center">
      <Container className="min-h-screen">
        <motion.div
          initial={{ opacity: 0, x: -50 }}
          animate={{ opacity: 1, x: 0 }}
          transition={{ duration: 0.3 }}
          className="flex items-center justify-center"
        >
          <Card className="flex w-full max-w-sm flex-col items-center bg-card/75 backdrop-blur-lg">
            <CardHeader className="self-start">
              <CardTitle>Cadastre-se</CardTitle>
              <CardDescription>
                Insira seu email e uma senha para se cadastrar na plataforma
              </CardDescription>
            </CardHeader>
            <CardContent className="w-full">
              <SignUpForm />
            </CardContent>
          </Card>
        </motion.div>
      </Container>
    </div>
  );
}
