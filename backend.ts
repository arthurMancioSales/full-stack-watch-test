import express from "express";
import dotenv from "dotenv";
import router from "./backend/router";
import cors from "cors";
import cookieParser from "cookie-parser";

dotenv.config();

const app = express();
const port = process.env.BACKEND_PORT;

const corsOptions = {
  origin: process.env.FRONTEND_ADDRESS,
  optionsSuccessStatus: 200,
  credentials: true,
};

app.use(express.json());
app.use(cors(corsOptions));
app.use(cookieParser(process.env.SESSION_SECRET));

app.use("/", express.static("./public"));
app.use("/", router);

app.use(function (req, res) {
  res.status(404).send("Resource not found");
});

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is running on port http://localhost:${port}`);
});
