import { Message } from "@/lib/message";
import { Request, Response } from "express";
import { ValidationChain, body, query, validationResult } from "express-validator";
import * as movieService from "../services/movieService";
import { movie, response } from "@/types";
import { Genres, Movie } from "@prisma/client";

export function validate(method: string) {
  switch (method) {
    case "getMovies": {
      return [
        query("skip", Message.missingEmail).exists().isInt({ min: 0 }).escape(),
        query("take", Message.missingPassword).exists().isInt({ min: 0 }).escape(),
      ];
    }
    case "updateMovie": {
      return [
        body("title", Message.missingTitle).exists().isString().escape(),
        body("year", Message.missingYear).exists().toInt().isInt({ min: 0 }),
        body("director", Message.missingDirector).exists().isString().escape(),
        body("poster", Message.missingPoster).exists().isString().escape(),
        body("genres", Message.missingGenre).exists().isArray(),
        body("synopsis", Message.missingSynopsis).exists().isString().escape(),
        body("id", Message.missingId).exists().isString().escape(),
      ];
    }
    case "createMovie": {
      return [
        body("title", Message.missingTitle).exists().isString().escape(),
        body("year", Message.missingYear).exists().toInt().isInt({ min: 0 }),
        body("director", Message.missingDirector).exists().isString().escape(),
        body("poster", Message.missingPoster).exists().isString().escape(),
        body("genres", Message.missingGenre).exists().isArray(),
        body("synopsis", Message.missingSynopsis).exists().isString().escape(),
      ];
    }
    case "deleteMovie": {
      return [body("movieId", Message.missingId).exists().isString().escape()];
    }
  }
  return [] as ValidationChain[];
}

export async function getMovies(req: Request, res: Response) {
  try {
    const errors = await validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { skip, take } = req.query;

    const serviceResponse: response<{ movies: Partial<Movie>[] } | null> =
      await movieService.getMovies(parseInt(skip as string), parseInt(take as string));

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { movies: serviceResponse.data.movies },
      errors: [],
    } as response<{
      movies: Movie[];
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function getGenres(req: Request, res: Response) {
  try {
    const serviceResponse: response<{ genres: Genres[] } | null> = await movieService.getGenres();

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { genres: serviceResponse.data.genres },
      errors: [],
    } as response<{
      genres: Genres[];
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function updateMovie(req: Request, res: Response) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const movie: Partial<movie> = req.body;
    const genres: Genres["id"][] = req.body.genres;

    const serviceResponse: response<{ movie: Partial<Movie> } | null> =
      await movieService.updateMovie(movie, genres);

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { movie: serviceResponse.data.movie },
      errors: [],
    } as response<{
      movie: Movie;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function deleteMovie(req: Request, res: Response) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }
    const movieId: string = req.body.movieId;

    const serviceResponse: response<{ success: boolean }> = await movieService.deleteMovie(movieId);

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { sucess: serviceResponse.data.success },
      errors: [],
    } as response<{
      sucess: boolean;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function createMovie(req: Request, res: Response) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { title, synopsis, director, poster, year } = req.body;
    const genres: Genres["id"][] = req.body.genres;

    const serviceResponse: response<{ movie: Partial<Movie> } | null> =
      await movieService.createMovie({ title, synopsis, director, poster, year }, genres);

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { movie: serviceResponse.data.movie },
      errors: [],
    } as response<{
      movie: Movie;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}
