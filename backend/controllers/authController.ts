import { Message } from "@/lib/message";
import { Request, Response } from "express";
import { ValidationChain, body, validationResult } from "express-validator";
import * as authService from "../services/authService";
import { response } from "@/types";
import { User } from "@prisma/client";

export function validate(method: string) {
  switch (method) {
    case "signIn": {
      return [
        body("email", Message.missingEmail).exists().isEmail().normalizeEmail(),
        body("password", Message.missingPassword).exists().isString().escape(),
      ];
    }
    case "signUp": {
      return [
        body("email", Message.missingEmail).exists().isEmail().normalizeEmail(),
        body("password", Message.missingPassword).exists().isString().escape(),
        body("passwordConfirmation", Message.missingPassword).exists().isString().escape(),
      ];
    }
    case "readUser": {
      return [body("id", Message.missingId).exists().isString().escape()];
    }
    case "updateUser": {
      return [
        body("id", Message.missingId).exists().isString().escape(),
        body("email", Message.missingEmail).exists().isEmail().normalizeEmail(),
        body("oldPassword", Message.missingPassword).exists().isString().escape(),
        body("newPassword", Message.missingPassword).exists().isString().escape(),
      ];
    }
    case "deleteUser": {
      return [
        body("id", Message.missingId).exists().isString().escape(),
        body("password", Message.missingPassword).exists().isString().escape(),
      ];
    }
  }
  return [] as ValidationChain[];
}

export async function signIn(req: Request, res: Response) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { email, password } = req.body;

    const serviceResponse: response<{ token: string } | null> = await authService.signIn(
      email,
      password,
    );

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { token: serviceResponse.data.token },
      errors: [],
    } as response<{
      token: string;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function signUp(req: Request, res: Response) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { email, password, passwordConfirmation } = req.body;

    if (password !== passwordConfirmation) {
      return res
        .status(400)
        .json({ data: null, errors: [{ msg: Message.passwordMismatch }] } as response<null>);
    }

    const passwordValidator = /^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/;

    if (!passwordValidator.test(password)) {
      return res
        .status(400)
        .json({ data: null, errors: [{ msg: Message.passwordTooWeak }] } as response<null>);
    }

    const serviceResponse: response<{ token: string } | null> = await authService.signUp(
      email,
      password,
    );

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { token: serviceResponse.data.token },
      errors: [],
    } as response<{
      token: string;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function readUser(req: Request, res: Response) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { id } = req.body;

    const serviceResponse: response<Partial<User> | null> = await authService.getUserById(id);

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: serviceResponse.data,
      errors: [],
    } as response<User>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function updateUser(req: Request, res: Response) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { id, email, oldPassword, newPassword } = req.body;

    const serviceResponse: response<{ token: string } | null> = await authService.updateUser(
      id,
      email,
      oldPassword,
      newPassword,
    );

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { token: serviceResponse.data.token },
      errors: [],
    } as response<{
      token: string;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}

export async function deleteUser(req: Request, res: Response) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ data: null, errors: errors.array() } as response<null>);
    }

    const { id, password } = req.body;

    const serviceResponse: response<{ success: boolean } | null> = await authService.deleteUser(
      id,
      password,
    );

    if (!serviceResponse.data) {
      return res.status(400).json(serviceResponse);
    }

    return res.status(200).json({
      data: { success: serviceResponse.data.success },
      errors: [],
    } as response<{
      success: boolean;
    }>);
  } catch (error) {
    return res
      .status(500)
      .json({ data: null, errors: [{ msg: Message.unexpectedError }] } as response<null>);
  }
}
