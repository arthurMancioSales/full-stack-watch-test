import multer from "multer";
import { v4 as uuid } from "uuid";
import path from "path";

const storage = multer.diskStorage({
  destination: "./public/uploads",

  filename: function (req, file, cb) {
    const fileExtension = path.extname(file.originalname);

    const filename = `${uuid()}${fileExtension}`;
    cb(null, filename);
  },
});

const upload = multer({ storage }).single("upload");

export default upload;
