import jwt from "jsonwebtoken";
import { config } from "dotenv";
import { NextFunction, Request, Response } from "express";
import { Message } from "@/lib/message";
import { response } from "@/types";

config();

export default function authenticateSession(req: Request, res: Response, next: NextFunction) {
  const tokenSecret = process.env.SESSION_SECRET;

  if (!tokenSecret) {
    throw new Error("Token secret is not defined");
  }

  try {
    jwt.verify(req.cookies.session, tokenSecret);
    next();
  } catch (error) {
    res.status(400).json({ data: null, errors: [{ msg: Message.accessDenied }] } as response<null>);
  }
}
