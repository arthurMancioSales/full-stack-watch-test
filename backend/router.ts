import express, { Request, Response } from "express";
import { router as authRouter } from "./routes/auth";
import { router as movieRouter } from "./routes/movies";
import authenticateSession from "./lib/authenticationMiddleware";
import { response } from "@/types";
import { Message } from "@/lib/message";
import upload from "./lib/upload";

const router = express.Router();

router.use("/session", authRouter);

router.use("/movies", authenticateSession, movieRouter);

router.post("/upload", authenticateSession, upload, (req: Request, res: Response) => {
  if (req.file) {
    const uploadedFile = req.file;

    return res.status(200).json({
      data: { uploadedFile: `/uploads/${uploadedFile?.filename}` },
      errors: [],
    } as response<{
      uploadedFile: string;
    }>);
  }

  return res.status(400).json({
    data: null,
    errors: [{ msg: Message.errorOnFileUpload }],
  } as response<null>);
});

export default router;
