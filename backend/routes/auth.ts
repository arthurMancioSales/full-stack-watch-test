import express, { Request, Response } from "express";
import * as authController from "../controllers/authController";
import authenticateSession from "../lib/authenticationMiddleware";

export const router = express.Router();

router.post("/sign-in", authController.validate("signIn"), authController.signIn);

router.post("/sign-up", authController.validate("signUp"), authController.signUp);

router.get("/validate-session", authenticateSession, (req: Request, res: Response) => {
  res.status(200).json({ data: { isValid: true }, errors: [] });
});

router.get(
  "/read-user",
  authenticateSession,
  authController.validate("readUser"),
  authController.readUser,
);

router.post(
  "/update-user",
  authenticateSession,
  authController.validate("updateUser"),
  authController.updateUser,
);

router.delete(
  "/delete-user",
  authenticateSession,
  authController.validate("deleteUser"),
  authController.deleteUser,
);
