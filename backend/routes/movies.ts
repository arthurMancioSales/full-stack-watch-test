import express from "express";
import * as movieController from "../controllers/movieController";

export const router = express.Router();

router.get("/get-movies", movieController.validate("getMovies"), movieController.getMovies);

router.get("/get-genres", movieController.getGenres);

router.post("/update-movie", movieController.validate("updateMovie"), movieController.updateMovie);

router.post("/create-movie", movieController.validate("createMovie"), movieController.createMovie);

router.delete(
  "/delete-movie",
  movieController.validate("deleteMovie"),
  movieController.deleteMovie,
);
