import { PrismaClient, User } from "@prisma/client";

const prisma = new PrismaClient();

export async function signUp(email: string, password: string) {
  const user = await prisma.user.create({
    data: {
      email,
      password,
    },
  });

  return user;
}

export async function getUser(email: string) {
  const user = await prisma.user.findUnique({
    where: {
      email,
    },
  });

  return user;
}

export async function getUserById(id: string) {
  const user = await prisma.user.findUnique({
    where: {
      id,
    },
  });
  return user;
}

export async function updateUser(id: string, data: Partial<User>) {
  const user = await prisma.user.update({
    where: {
      id,
    },
    data,
  });
  return user;
}

export async function deleteUser(id: string) {
  const user = await prisma.user.delete({
    where: {
      id,
    },
  });
  return user;
}
