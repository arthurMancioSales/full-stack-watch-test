import { movie } from "@/types";
import { PrismaClient, Genres } from "@prisma/client";

const prisma = new PrismaClient();

export async function getMovies(skip: number, take: number) {
  const movies = await prisma.movie.findMany({
    skip,
    take,
    select: {
      id: true,
      title: true,
      year: true,
      genres: {
        select: {
          id: true,
          name: true,
        },
      },
      director: true,
      poster: true,
      synopsis: true,
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  return movies;
}

export async function createMovie(
  {
    title,
    year,
    director,
    poster,
    synopsis,
  }: { title: string; year: number; director: string; poster: string; synopsis: string },
  genres: Genres["id"][],
) {
  const movie = await prisma.movie.create({
    data: {
      title,
      year,
      genres: {
        connect: genres.map((genre) => ({ id: genre })),
      },
      director,
      poster,
      synopsis,
    },
    select: {
      id: true,
      title: true,
      year: true,
      genres: true,
      director: true,
    },
  });

  return movie;
}

export async function getGenres() {
  const movie = await prisma.genres.findMany();

  return movie;
}

export async function getMovieByTitle(title: string | undefined) {
  const movie = await prisma.movie.findUnique({
    where: { title },
  });

  return movie;
}

export async function updateMovie(movie: Partial<movie>, genres: Genres["id"][]) {
  const updatedMovie = await prisma.movie.update({
    where: { id: movie.id },
    data: {
      title: movie.title,
      year: movie.year,
      genres: {
        connect: genres.map((genre) => ({ id: genre })),
      },
      director: movie.director,
      poster: movie.poster,
      synopsis: movie.synopsis,
    },
    select: {
      id: true,
      title: true,
      year: true,
      genres: true,
      director: true,
    },
  });
  return updatedMovie;
}

export async function deleteMovie(movieId: string) {
  const deletedMovie = await prisma.movie.delete({
    where: { id: movieId },
  });

  return deletedMovie;
}
