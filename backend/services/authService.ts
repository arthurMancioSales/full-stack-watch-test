import { Message } from "@/lib/message";
import * as authRepository from "../repositories/authRepository";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

export async function signIn(email: string, password: string) {
  const user = await authRepository.getUser(email);

  if (!user) {
    return { data: null, errors: [{ msg: Message.userNotFound }] };
  }

  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    return { data: null, errors: [{ msg: Message.invalidCredentials }] };
  }

  const tokenSecret = process.env.SESSION_SECRET;

  if (!tokenSecret) {
    throw new Error("Token secret is not defined");
  }

  const token = jwt.sign({ email: user.email, id: user.id }, tokenSecret);

  return { data: { token }, errors: [] };
}

export async function signUp(email: string, password: string) {
  const existingUser = await authRepository.getUser(email);

  if (existingUser) {
    return { data: null, errors: [{ msg: Message.emailAlreadyExists }] };
  }

  const hashedPassword = await bcrypt.hash(password, 10);
  const user = await authRepository.signUp(email, hashedPassword);

  const tokenSecret = process.env.SESSION_SECRET;

  if (!tokenSecret) {
    throw new Error("Token secret is not defined");
  }

  const token = jwt.sign({ email: user.email, id: user.id }, tokenSecret);

  return { data: { token }, errors: [] };
}

export async function getUserById(id: string) {
  const user = await authRepository.getUserById(id);

  if (!user) {
    return { data: null, errors: [{ msg: Message.userNotFound }] };
  }

  if (user.id !== id) {
    return { data: null, errors: [{ msg: Message.unauthorized }] };
  }

  return { data: user, errors: [] };
}

export async function updateUser(
  id: string,
  email: string,
  oldPassword: string,
  newPassword: string,
) {
  const existingUser = await authRepository.getUserById(id);

  if (!existingUser) {
    return { data: null, errors: [{ msg: Message.userNotFound }] };
  }

  if (existingUser.id !== id) {
    return { data: null, errors: [{ msg: Message.unauthorized }] };
  }

  const passwordMatch = await bcrypt.compare(oldPassword, existingUser.password);

  if (!passwordMatch) {
    return { data: null, errors: [{ msg: Message.invalidCredentials }] };
  }

  const updatedUser = await authRepository.updateUser(id, {
    email,
    password: newPassword ? await bcrypt.hash(newPassword, 10) : undefined,
  });

  const tokenSecret = process.env.SESSION_SECRET;

  if (!tokenSecret) {
    throw new Error("Token secret is not defined");
  }

  const token = jwt.sign({ email: updatedUser.email, id: updatedUser.id }, tokenSecret);

  return { data: { token }, errors: [] };
}

export async function deleteUser(id: string, password: string) {
  const existingUser = await authRepository.getUserById(id);

  if (!existingUser) {
    return { data: null, errors: [{ msg: Message.userNotFound }] };
  }

  if (existingUser.id !== id) {
    return { data: null, errors: [{ msg: Message.unauthorized }] };
  }

  const passwordMatch = await bcrypt.compare(password, existingUser.password);
  if (!passwordMatch) {
    return { data: null, errors: [{ msg: Message.invalidCredentials }] };
  }

  await authRepository.deleteUser(id);

  return { data: { success: true }, errors: [] };
}
