import { Message } from "@/lib/message";
import * as movieRepository from "../repositories/movieRepository";
import { movie, response } from "@/types";
import { Genres } from "@prisma/client";

export async function getMovies(skip: number, take: number) {
  const movies = await movieRepository.getMovies(skip, take);

  if (!movies) {
    return { data: null, errors: [{ msg: Message.noMovieFound }] };
  }

  return { data: { movies }, errors: [] };
}
export async function getGenres() {
  const genres = await movieRepository.getGenres();

  if (!genres) {
    return { data: null, errors: [{ msg: Message.noGenreFound }] };
  }

  return { data: { genres }, errors: [] };
}

export async function updateMovie(movie: Partial<movie>, genres: Genres["id"][]) {
  const existingMovie = await movieRepository.getMovieByTitle(movie.title);

  if (existingMovie) {
    return { data: null, errors: [{ msg: Message.movieAlreadyExists }] };
  }

  const updatedMovie = await movieRepository.updateMovie(movie, genres);
  if (!updatedMovie) {
    return { data: null, errors: [{ msg: Message.movieNotFound }] };
  }
  return { data: { movie: updatedMovie }, errors: [] };
}

export async function deleteMovie(movieId: string) {
  const deletedMovie = await movieRepository.deleteMovie(movieId);

  if (!deletedMovie) {
    return { data: null, errors: [{ msg: Message.movieNotFound }] };
  }
  return { data: { success: true }, errors: [] } as response<{ success: boolean }>;
}

export async function createMovie(
  {
    title,
    synopsis,
    director,
    poster,
    year,
  }: { title: string; year: number; director: string; poster: string; synopsis: string },
  genres: Genres["id"][],
) {
  const existingMovie = await movieRepository.getMovieByTitle(title);

  if (existingMovie) {
    return { data: null, errors: [{ msg: Message.movieAlreadyExists }] };
  }

  const createdMovie = await movieRepository.createMovie(
    { title, synopsis, director, poster, year },
    genres,
  );

  if (!createdMovie) {
    return { data: null, errors: [{ msg: Message.movieCreationFailed }] };
  }
  return { data: { movie: createdMovie }, errors: [] };
}
