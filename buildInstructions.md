# Build Instructions

To build the project, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Copy the env.example file and fill it.

4. Install the necessary dependencies by running the following command:

```bash
npm install
```

5. Build the frontend by running the following command:

```bash
npm run build
```

6. Once the build process is complete, you can start the frontend by runinng:

```bash
npm run start
```

7. Start the backend by runing the following command:

```bash
docker compose -f docker-compose.dev.yml up -d --build
```

8. Agora você está pronto para testar o projeto!