/*
  Warnings:

  - Added the required column `synopsis` to the `movie` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `movie` ADD COLUMN `synopsis` VARCHAR(191) NOT NULL;
