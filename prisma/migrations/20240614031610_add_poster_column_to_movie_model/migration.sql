/*
  Warnings:

  - Added the required column `poster` to the `movie` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `movie` ADD COLUMN `poster` VARCHAR(191) NOT NULL;
