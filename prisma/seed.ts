import { PrismaClient } from "@prisma/client";
import { faker } from "@faker-js/faker";
import { genresList } from "../src/types";
const prisma = new PrismaClient();

async function main() {
  const initialMoviesAmount = 20;
  const allGenres = Object.values(genresList);

  const seededGenres: { id: string }[] = [];

  for (const genre of allGenres) {
    seededGenres.push(
      await prisma.genres.upsert({
        where: { name: genre },
        update: {},
        create: { name: genre },
        select: { id: true },
      }),
    );
  }

  for (let i = 0; i < initialMoviesAmount; i++) {
    await prisma.movie.upsert({
      where: { title: `Filme ${i}` },
      update: {},
      create: {
        title: `Filme ${i}`,
        year: faker.date.past({ years: 10 }).getFullYear(),
        genres: {
          connect: faker.helpers.arrayElements(
            seededGenres,
            faker.number.int({ min: 0, max: seededGenres.length - 1 }),
          ),
        },
        director: faker.person.fullName(),
        synopsis: faker.lorem.paragraph(3),
        poster: faker.image.urlLoremFlickr(),
      },
    });
  }
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    // eslint-disable-next-line no-console
    console.log(e);
    await prisma.$disconnect();
    process.exit(1);
  });
