FROM node:latest

USER node

WORKDIR /home/node/app

COPY --chown=node:node . .

RUN npm install

RUN chmod +x entrypoint.sh

EXPOSE 8081

ENV BACKEND_PORT 8081
ENV HOSTNAME localhost

CMD ["./entrypoint.sh"]